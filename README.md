# Setting up a Db2 for LUW v11.1 virtual machine

## Prerequisites

CentOS 7 minimal ISO image from [here](https://www.centos.org/download/).

Db2 11.1 Developer-C tarball from [here](https://www.ibm.com/ca-en/marketplace/ibm-db2-direct-and-developer-editions).

## Creating a VM

Create a new VM using the CentOS 7 image. For convenience create a non-root user,
e.g. "vmuser", that is in the `sudoers` file with no password (unless you prefer to enter the
password each time you `sudo`). Upload an SSH public key for "vmuser".

From here you can run the provided Ansible playbook or use the manual procedure
explained later.

## Running the Ansible playbook

1. Update the `inventory` file by recording the actual IP address of your running
   VM. You can modify the instance and fenced user password variables, as well as other
   variables, if desired.

2. Update the Ansible variables at the top of `setup-db2-instance.yaml` to appropriate values:

   - Set `tarball_name` to the actual name of the Db2 installation archive if different.

   - Set `tarball_dir` to the path where `tarball_name` resides on your host machine (the one where Ansible runs).

   - If you'd rather download the Db2 tarball from an ftp server you own, modify `tarball_url` instead.

3. Run the playbook:

   `ansible-playbook -u vmuser -i inventory playbook.yaml` 

   `setup-db2-instance.yaml` will try to find the Db2 tarball in several places, 
   so don't be alarmed by the error messages at that point.

   The playbook runs a test script to confirm that the Db2 and PHP module installation
   succeeded.

4. For each new user run the playbook `setup-user.yaml`, providing the user name and password
   via variables on the command line, e.g.

   ```
   ansible-playbook -u vmuser -i inventory -e "user_name=test" \
   -e 'user_password=$6$salt$IxDD3jeSOb5eB1CX5LBsqZFVkJdido3OUILO5Ifz5iwMuTS4XMS130MTSuDDl3aCI6WouIL9AjRbLCelDCy.g.' \
   setup-user.yaml
   ```

   Notice that the password is supplied in the crypt format. You can use various methods to generate
   the encrypted password, e.g. `mkpasswd --method=sha-512` on Linux or 
   `python -c import crypt; print crypt.crypt("<password>","$6$<salt>")'` using Python.

## Performing manual Db2 installation

If you can't or don't want to use Ansible, you can perform the playbook steps manually.

1. Upload the Db2 tarball, presumably `v11.1_linuxx64_dec.tar.gz`, and `db2server.rsp` into `/tmp` on the virtual machine.

2. Log into the VM, change current directory to `/tmp` and unpack the tarball:

   `tar xzf v11.1_linuxx64_dec.tar.gz`

   This will create a directory `/tmp/server_dec` with all installation files.

3. If desired, edit `/tmp/db2server.rsp` and replace  values for `DB2_INST.PASSWORD` and `DB2_INST.FENCED_PASSWORD`. 

4. Install prerequisite packages:

   ```
   sudo yum install -y libaio ksh perl-Sys-Syslog
   sudo yum install -y gcc php-pear php-devel
   ```

5. Edit `/etc/hosts` (as root, obviously) and add a record mapping the VM default IP address to its short and full hostname, e.g.

   `172.16.161.128 db2server db2server.example.com`
   
   Check that both names resolve correctly by pinging them.

6. Change to the installation file directory `/tmp/server_dec` and run the installer:

   `sudo ./db2setup -r /tmp/db2server.rsp`

   You might see a couple of warnings about missing 32-bit "prerequisite" libraries, which can be safely ignored.
   Expected output at the end of installation is:

   ```
   DBI1191I  db2setup is installing and configuring DB2 according to the
      response file provided. Please wait.


   The execution completed successfully.
   ```

   The Db2 instance owner created by the installation is "db2inst1" with password "db2inst1". 
   Use `sudo su - db2inst1` to run Db2 commands. 


7. Open the firewall port for Db2 remote connections.

   ```
   sudo firewall-cmd --add-port 50000/tcp
   sudo firewall-cmd --runtime-to-permanent
   ```

   Use the actual Db2 port number if you modified it in `db2server.rsp`.

8. Install the PHP ibm_db2 module.

   `echo /opt/ibm/db2/V11.1 | pecl install ibm_db2`

   Once the installation completes, update `/etc/php.ini` and add these two lines:

   ```
   extension=ibm_db2.so
   ibm_db2.instance_name=db2inst1
   ```

   Use the actual Db2 installation directory and instance name if you modified
   them in `db2server.rsp`.

9. Create the database and setup role.

   This is done once per server.

   Log in as the instance owner, making sure the profile script is executed:

        sudo su - db2inst1

    Create the database:

        db2 create db fiddledb restrictive 

    Upload the role creation script `role_def.sql` and execute it (database name and role name
    are hard-coded in the script, edit as necessary).

        db2 -tf role_def.sql

10. Create users.
 
    This is done for each new user ID. 

    Db2 authenticates users against the underlying OS, which means every user ID
    that wants to connect to the database needs to be created in the OS first:

        sudo useradd fiddle

    Upload the user creation script `user_def.sql` and execute it (database name, 
    user name and role name are hard-coded in the script, edit as necessary).

        db2 -tf user_def.sql

    
## Appendices

### Files in this directory

- `db2server.rsp.j2` -- Db2 installation response file template for Ansible.
- `db2server.rsp`    -- Db2 installation response file for manual setup.
- `db2server11.rsp`  -- Full server response file for reference.
- `inventory`        -- Ansible inventory (sample).
- `README.md`        -- This document.
- `playbook.yaml`             -- Master playbook to set up the server and database. It
                                 calls `setup-db2-instance.yaml`, `setup-database.yaml`,
                                 and `setup-server.yaml`. This playbook should be used
                                 once per server.
- `setup-db2-instance.yaml`   -- Ansible playbook to set up Db2 11.1
- `setup-database.yaml`       -- Ansible playbook to create the database (work in progress)
- `setup-server.yaml`         -- Playbook to set up PHP and the Db2 module.
- `setup-user.yaml`           -- Playbook to create a user ID and grant appropriate privileges.
                                 This playbook should be used every time a new user is created.
- `test.php.j2`      -- PHP test script template to verify  ibm_db2 module installation.
- `role_def.sql.j2`  -- Role definition script template for use in `setup-database.yaml`
- `user_def.sql.j2`  -- User definition script template for use in `setup-user.yaml`
- `role_def.sql`,  `user_def.sql` -- Role and user definition scripts for manual installation.
