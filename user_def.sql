-- For each new user:
-- 1. Create OS user; 
-- Note: schema name must match user name
connect to fiddledb;
create schema fiddle;
grant createin on schema fiddle to user fiddle;
grant role fiddle_role to user fiddle;
commit;
