-- Create a role and grant common minimal privileges
connect to fiddledb;
create role fiddle_role;
grant connect, createtab on database to role fiddle_role; 
grant use of tablespace userspace1 to role fiddle_role;
grant usage on workload SYSDEFAULTUSERWORKLOAD to role fiddle_role;

-- The following catalog privileges are required in most cases for database metadata access:
-- per http://www-01.ibm.com/support/docview.wss?rs=71&uid=swg21267177
grant select on table SYSIBM.SQLTABLES to role fiddle_role;
grant select on table SYSIBM.SYSTABLES to role fiddle_role;
grant select on table SYSIBM.SQLTABLETYPES to role fiddle_role;
grant select on table SYSIBM.SQLTABLEPRIVILEGES to role fiddle_role;
grant select on table SYSIBM.SYSCOLUMNS to role fiddle_role;
grant select on table SYSIBM.SQLSPECIALCOLUMNS to role fiddle_role;
grant select on table SYSIBM.SQLCOLPRIVILEGES to role fiddle_role;
grant select on table SYSIBM.SYSDUMMY1 to role fiddle_role;
grant select on table SYSIBM.SQLPRIMARYKEYS to role fiddle_role;
grant select on table SYSIBM.SQLSTATISTICS to role fiddle_role;
grant select on table SYSIBM.SQLFOREIGNKEYS to role fiddle_role;
grant select on table SYSIBM.SYSINDEXES to role fiddle_role;
grant select on table SYSIBM.SQLPROCEDURES to role fiddle_role;
grant select on table SYSIBM.SQLPROCEDURECOLS to role fiddle_role;
grant select on table SYSIBM.SYSROUTINES to role fiddle_role;
grant select on table SYSIBM.SYSROUTINEPARMS to role fiddle_role;
grant select on table SYSIBM.SQLTYPEINFO to role fiddle_role;
grant select on table SYSIBM.SQLUDTS to role fiddle_role;
grant select on table SYSIBM.SQLSCHEMAS to role fiddle_role;
grant select on table SYSIBM.SYSRELS to role fiddle_role;
grant select on table SYSIBM.SYSKEYCOLUSE to role fiddle_role;
grant select on table SYSIBM.SYSTABCONST to role fiddle_role;
grant select on table SYSIBM.SYSDATATYPES to role fiddle_role;

-- These are CLP packages
grant execute on package NULLID.SQLC2O28 to role fiddle_role;
grant execute on package NULLID.SQLC3O27 to role fiddle_role;
grant execute on package NULLID.SQLC4O27 to role fiddle_role;
grant execute on package NULLID.SQLC5O27 to role fiddle_role;
grant execute on package NULLID.SQLC6O27 to role fiddle_role;

commit;

